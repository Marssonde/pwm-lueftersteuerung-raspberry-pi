#!/bin/bash
# Lüftersteuerung
GPIO=12
frequency=50
# Celsius * 1000
THRESHOLD_MIN=47500
THRESHOLD_MAX=60000
PULSEMODE_MIN=500000
PULSEMODE_MAX=1000000
fanspeed=$(pigs gdc $GPIO)
CPU_TEMP=$(cat /sys/class/thermal/thermal_zone0/temp)
cd /var/www/temperature/
sqlite3 main.db <<EOF
INSERT INTO MAIN (TIMESTAMP,TEMPERATURE)
VALUES (strftime('%s','now'), $CPU_TEMP );
.exit
EOF
# y= 4*x-140 => der lineare Graph zur Berechnung der Pulsweite 50 Grad Celsius entspricht 60% Fanspeed und 60 Grad Celsius entspricht 100% Fanspeed
tmppulsemode=$(echo "scale=3;(4*$CPU_TEMP/1000-140)*10000" | bc)
# $tmppulsemode/1 => Umwandung in Integer Zahl ohne Nachkommastellen
pulsemode=$(echo "scale=0;$tmppulsemode/1" | bc)


if [ $CPU_TEMP -ge $THRESHOLD_MAX ]
  then
    if [ $fanspeed != $PULSEMODE_MAX ] 
      then
        echo "100" > /var/www/temperature/fan-status
        pigs hp $GPIO $frequency $PULSEMODE_MAX
      fi
elif [ $CPU_TEMP -le $THRESHOLD_MIN ]
  then
    if [ $fanspeed != $PULSEMODE_MIN ] 
      then
        echo "50" > /var/www/temperature/fan-status
        pigs hp $GPIO $frequency $PULSEMODE_MIN
      fi
else
    if [ $fanspeed != $pulsemode ] 
      then
        echo $(($pulsemode/10000)) > /var/www/temperature/fan-status
        pigs hp $GPIO $frequency $pulsemode
      fi
fi
 

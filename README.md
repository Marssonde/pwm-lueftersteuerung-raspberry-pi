# pwm-lueftersteuerung-raspberry-pi
  <p>Voraussetzung:</p><p>&nbsp;&nbsp;&nbsp; Raspberry Pi 3 oder 4<br> &nbsp;&nbsp;&nbsp; USB Netzteil für Raspberry Pi 3 oder 4<br> &nbsp;&nbsp;&nbsp; Gehäuse für Raspberry Pi 3 oder 4<br> &nbsp;&nbsp;&nbsp; SD-Card oder USB Stick¹ mit mindestens 16 GB<br> &nbsp;&nbsp;&nbsp; Ethernet-Kabel<br> &nbsp;&nbsp;&nbsp; Raspberry Pi OS with desktop or Lite<br> &nbsp;&nbsp;&nbsp; Download: <a href="https://www.raspberrypi.com/software/operating-systems/#raspberry-pi-os-32-bit" target="_blank" rel="noreferrer">https://www.raspberrypi.com/software/operating-systems/#raspberry-pi-os-32-bit</a><br> &nbsp;&nbsp;&nbsp; Dokumentation: <a href="https://www.raspberrypi.com/documentation/computers/getting-started.html#setting-up-your-raspberry-pi" target="_blank" rel="noreferrer">https://www.raspberrypi.com/documentation/computers/getting-started.html#setting-up-your-raspberry-pi</a><br> 1:= Das Booten vom USB Stick ist nicht immer möglich.</p></div>
  <p>Viele Gehäuse für den Raspberry Pi haben einen eingebauten Lüfter der meistens mit 5V betrieben wird. Wenn er ständig läuft, kann das manchmal nervigen Lärm verursachen. Es ist möglich den Lüfter an die 3,3 V Leitung anzuschließen, dadurch kann man den Lüfter leiser machen.&nbsp; Oder man bastelt eine elektronische Lüftersteuerung. Dazu benötigt man folgende Bauteile:</p><div class="table-responsive"><table class="table table-bordered table-striped"><tbody><tr><td>Menge</td><td>Bauteil</td><td>Wert</td><td>Beschreibung</td></tr><tr><td>1</td><td>Widerstand</td><td>1 kΩ</td><td><a href="https://components101.com/resistors/resistor" target="_blank" rel="noreferrer">https://components101.com/resistors/resistor</a></td></tr><tr><td>1</td><td>Transistor</td><td>2N2222A</td><td><a href="https://components101.com/transistors/2n2222a-pinout-equivalent-datasheet" target="_blank" rel="noreferrer">https://components101.com/transistors/2n2222a-pinout-equivalent-datasheet</a></td></tr></tbody></table></div><div class="table-responsive"><table class="table table-bordered table-striped"><tbody><tr><td>Plan</td><td>Download</td><td>Quelle</td></tr><tr><td>GPIO Pinbelegung</td><td><a href="/fileadmin/user_upload/mamuck.de/Images/GPIO-Pinout-Diagram-2.png">Download</a></td><td><p>Quelle:<br><a href="https://www.raspberrypi.com/documentation/computers/os.html#gpio-and-the-40-pin-header" target="_blank" rel="noreferrer">https://www.raspberrypi.com/documentation/computers/os.html#gpio-and-the-40-pin-header</a></p></td></tr><tr><td>Schaltplan</td><td><a href="/fileadmin/user_upload/mamuck.de/Images/raspbyfan-pwm.png">Download</a></td><td>&nbsp;</td></tr></tbody></table></div><h4>pigpio library installieren</h4><p>&nbsp;</p>

    
   
sudo apt-get update && sudo apt-get upgrade
sudo apt install python-setuptools python3-setuptools
sudo apt install pigpio python-pigpio python3-pigpio
sudo systemctl start pigpiod
sudo systemctl enable pigpiod
    

    Die Steuerung wird mit dem folgenden Skript realisiert:
 
    
nano cooler_fan.sh
  

#!/bin/bash
# Lüftersteuerung
GPIO=12
frequency=50
# Celsius * 1000
THRESHOLD_MIN=47500
THRESHOLD_MAX=60000
PULSEMODE_MIN=500000
PULSEMODE_MAX=1000000
fanspeed=$(pigs gdc $GPIO)
CPU_TEMP=$(cat /sys/class/thermal/thermal_zone0/temp)

# y= 4*x-140 => der lineare Graph zur Berechnung der Pulsweite 
# 50 Grad Celsius entspricht 60% Fanspeed und 60 Grad Celsius entspricht 100% Fanspeed
# scale=3 => 3 Nachkommastellen
tmppulsemode=$(echo "scale=3;(4*$CPU_TEMP/1000-140)*10000" | bc)
# $tmppulsemode/1 => Umwandung in Integer Zahl ohne Nachkommastellen
pulsemode=$(echo "scale=0;$tmppulsemode/1" | bc)
if [ $CPU_TEMP -ge $THRESHOLD_MAX ]
  then
    if [ $fanspeed != $PULSEMODE_MAX ] 
      then
        echo "100" > /var/www/temperature/fan-status
        pigs hp $GPIO $frequency $PULSEMODE_MAX
      fi
elif [ $CPU_TEMP -le $THRESHOLD_MIN ]
  then
    if [ $fanspeed != $PULSEMODE_MIN ] 
      then
        echo "50" > /var/www/temperature/fan-status
        pigs hp $GPIO $frequency $PULSEMODE_MIN
      fi
else
    if [ $fanspeed != $pulsemode ] 
      then
        echo $(($pulsemode/10000)) > /var/www/temperature/fan-status
        pigs hp $GPIO $frequency $pulsemode
      fi
fi
 
  Das Skript ausführbar machen:

  <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
  <div class="card-header">Terminal</div>
  <div class="card-body">
    
   
chmod +x fancontrol.py
    
  
  Nun noch einen Cronjob erstellen:
 
crontab -e

# Edit this file to introduce tasks to be run by cron.
# 
# Each task to run has to be defined through a single line
# indicating with different fields when the task will be run
# and what command to run for the task
# 
# To define the time you can provide concrete values for
# minute (m), hour (h), day of month (dom), month (mon),
# and day of week (dow) or use '*' in these fields (for 'any').
# 
# Notice that tasks will be started based on the cron's system
# daemon's notion of time and timezones.
# 
# Output of the crontab jobs (including errors) is sent through
# email to the user the crontab file belongs to (unless redirected).
# 
# For example, you can run a backup of all your user accounts
# at 5 a.m every week with:
# 0 5 * * 1 tar -zcf /var/backups/home.tgz /home/
# 
# For more information see the manual pages of crontab(5) and cron(8)
# 
# m h  dom mon dow   command
*/5 * * * * /home/#USER#/cooler_fan.sh > /dev/null

    
